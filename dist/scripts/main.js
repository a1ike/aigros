"use strict";

$(document).ready(function () {
  /* $('a[href^="#"]').on('click', function (e) {
    e.preventDefault();
    var target = this.hash,
      $target = $(target);
      $('html, body').stop().animate({
      'scrollTop': $target.offset().top
    }, 900, 'swing', function () {
      window.location.hash = target;
    });
  }); */
  $('.phone').inputmask('+7(999)999-99-99');
  $('.a-header__nav').on('click', function (e) {
    e.preventDefault();
    $('.a-nav').slideToggle('fast', function (e) {// callback
    });
  });
  $('.a-nav__close').on('click', function (e) {
    e.preventDefault();
    $('.a-nav').slideToggle('fast', function (e) {// callback
    });
  });
  $('.a-nav__contact-front').on('click', function (e) {
    e.preventDefault();
    $('.a-nav__contact-front').toggle();
    $('.a-nav__contact-back').toggle();
  });
  $('.a-nav__contact-close').on('click', function (e) {
    e.preventDefault();
    $('.a-nav__contact-back').toggle();
    $('.a-nav__contact-front').toggle();
  });
  $('.open-modal').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function (e) {// callback
    });
  });
  $('.a-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.a-modal').slideToggle('fast', function (e) {// callback
    });
  });
  $('.open-search').on('click', function (e) {
    e.preventDefault();
    $('.a-search').slideToggle('fast', function (e) {// callback
    });
  });
  $('.a-search__close').on('click', function (e) {
    e.preventDefault();
    $('.a-search').slideToggle('fast', function (e) {// callback
    });
  });
  $('.a-sidebar__collapse-header').on('click', function (e) {
    e.preventDefault();
    $(this).next().slideToggle('fast', function (e) {
      $(this).prev().find('.a-sidebar__collapse-arrow').toggleClass('a-sidebar__collapse-arrow_opened');
    });
  });
  $('.a-sidebar__title').on('click', function (e) {
    e.preventDefault();
    $(this).parent().next().slideToggle('fast');
  });
  $('.a-home-new').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false,
    dots: false,
    centerMode: false,

    /* prevArrow: $('.a-new-slick_left'),
    nextArrow: $('.a-new-slick_right'), */
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: true,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.a-mob-new-left'),
        nextArrow: $('.a-mob-new-right'),
        speed: 500
      }
    }]
  });
  $('.a-home-new-2').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false,
    dots: false,
    centerMode: false,

    /* prevArrow: $('.a-new-slick_left'),
    nextArrow: $('.a-new-slick_right'), */
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: true,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.a-mob-special-left'),
        nextArrow: $('.a-mob-special-right'),
        speed: 500
      }
    }]
  });
  $('.a-new-slick_left').click(function () {
    $('.a-home-new').slick('slickPrev');
    $('.a-home-new-2').slick('slickPrev');
  });
  $('.a-new-slick_right').click(function () {
    $('.a-home-new').slick('slickNext');
    $('.a-home-new-2').slick('slickNext');
  });
  $('.a-home-special').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false,
    dots: false,
    centerMode: false,

    /* prevArrow: $('.a-special-slick_left'),
    nextArrow: $('.a-special-slick_right'), */
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: true,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.a-mob-new-left-2'),
        nextArrow: $('.a-mob-new-right-2'),
        speed: 500
      }
    }]
  });
  $('.a-home-special-2').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    arrows: false,
    dots: false,
    centerMode: false,

    /* prevArrow: $('.a-special-slick_left'),
    nextArrow: $('.a-special-slick_right'), */
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: true,
        infinite: true,
        centerMode: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: $('.a-mob-special-left-2'),
        nextArrow: $('.a-mob-special-right-2'),
        speed: 500
      }
    }]
  });
  $('.a-special-slick_left').click(function () {
    $('.a-home-special').slick('slickPrev');
    $('.a-home-special-2').slick('slickPrev');
  });
  $('.a-special-slick_right').click(function () {
    $('.a-home-special').slick('slickNext');
    $('.a-home-special-2').slick('slickNext');
  });
  $(window).on('load resize', function (e) {
    if ($(window).width() < 1200) {
      var newContent = $('.a-home-new-2').detach();
      newContent.insertAfter($('#help-profit'));
      var profitContent = $('.a-home-special').detach();
      profitContent.insertAfter($('#help-new-2'));
      $('.a-home-new').slick('reInit');
      $('.a-home-new-2').slick('reInit');
      $('.a-home-special').slick('reInit');
      $('.a-home-special-2').slick('reInit');
      /* var profitContent = $('#itemsProfit').detach();
      profitContent.insertAfter($('#itemsNew')); */

      /* $('.a-good-cards').slick({
        dots: false,
        arrows: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        responsive: [{
            breakpoint: 9999,
            settings: 'unslick'
          },
          {
            breakpoint: 1200,
            settings: {
              dots: false,
              arrows: true,
              infinite: true,
              speed: 500,
              slidesToShow: 1,
              slidesToScroll: 1,
              autoplay: true,
              autoplaySpeed: 5000
            }
          }
        ]
      }); */
    }

    $('.a-home-new').slick('reInit');
    $('.a-home-new-2').slick('reInit');
    $('.a-home-special').slick('reInit');
    $('.a-home-special-2').slick('reInit');
  });
  $('.a-clients').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    arrows: true,
    dots: false,
    centerMode: false,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: false,
        infinite: true,
        centerMode: true,
        slidesToShow: 2,
        speed: 500
      }
    }]
  });
  $('.a-brand__input-minus').on('click', function (e) {
    var currentVal = $(this).next().val();

    if (currentVal > 0) {
      $(this).next().val(+currentVal - 1);
    }
  });
  $('.a-brand__input-plus').on('click', function (e) {
    var currentVal = $(this).prev().val();
    $(this).prev().val(+currentVal + 1);
  });
  $('.a-brand__big').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '.a-brand__small'
  });
  $('.a-brand__small').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.a-brand__big',
    arrows: true,
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [{
      breakpoint: 1200,
      settings: {
        dots: false,
        arrows: false,
        infinite: true,
        centerMode: false,
        speed: 500
      }
    }]
  });
  $('#lot-to-form').click(function () {
    $([document.documentElement, document.body]).animate({
      scrollTop: $('#lot-form').offset().top
    }, 500);
  });
  $(window).on('load resize scroll', function (e) {
    if ($(window).width() > 1200) {
      $('.slick-current .a-brand__big-card-image').elevateZoom({
        zoomWindowPosition: 'a-brand__outer',
        borderSize: 1,
        easing: true
      });
      $('.a-brand__big').on('beforeChange', function () {
        $.removeData($('img'), 'elevateZoom');
        $('.zoomContainer').remove();
      });
      $('.a-brand__big').on('afterChange', function () {
        $('.slick-current .a-brand__big-card-image').elevateZoom({
          zoomWindowPosition: 'a-brand__outer',
          borderSize: 1,
          easing: true
        });
      });
    } else {
      $('.a-good-line__left').addClass('a-good-card__header');
      $('.a-good-line__left').removeClass('a-good-line__left');
      $('.a-good-line__content').addClass('a-good-card__content');
      $('.a-good-line__content').removeClass('a-good-line__content');
      $('.a-good-line_toggled_row').addClass('a-good-card__row');
      $('.a-good-line_toggled_row').addClass('a-good-card__row-middle');
      $('.a-good-line_toggled_row').removeClass('a-good-line_toggled_row');
      $('.a-good-line_toggled_prices').addClass('a-good-card__prices');
      $('.a-good-line_toggled_prices').removeClass('a-good-line_toggled_prices');
      $('.a-good-card__buttons').removeClass('a-good-line_toggled_buttons');
      $('.a-good-line').addClass('a-good-card');
      $('.a-good-line').removeClass('a-good-line');
    }
  }); // Iterate over each select element

  $('.selectbox').each(function () {
    // Cache the number of options
    var $this = $(this),
        numberOfOptions = $(this).children('option').length; // Hides the select element

    $this.addClass('s-hidden'); // Wrap the select element in a div

    $this.wrap('<div class="select"></div>'); // Insert a styled div to sit over the top of the hidden select element

    $this.after('<div class="styledSelect"></div>'); // Cache the styled div

    var $styledSelect = $this.next('div.styledSelect'); // Show the first select option in the styled div

    $styledSelect.text($this.children('option').eq(0).text()); // Insert an unordered list after the styled div and also cache the list

    var $list = $('<ul />', {
      'class': 'options'
    }).insertAfter($styledSelect); // Insert a list item into the unordered list for each select option

    for (var i = 0; i < numberOfOptions; i++) {
      $('<li />', {
        text: $this.children('option').eq(i).text(),
        rel: $this.children('option').eq(i).val()
      }).appendTo($list);
    } // Cache the list items


    var $listItems = $list.children('li'); // Show the unordered list when the styled div is clicked (also hides it if the div is clicked again)

    $styledSelect.click(function (e) {
      e.stopPropagation();
      $('div.styledSelect.active').each(function () {
        $(this).removeClass('active').next('ul.options').hide();
      });
      $(this).toggleClass('active').next('ul.options').toggle();
    }); // Hides the unordered list when a list item is clicked and updates the styled div to show the selected list item
    // Updates the select element to have the value of the equivalent option

    $listItems.click(function (e) {
      e.stopPropagation();
      $styledSelect.text($(this).text()).removeClass('active');
      $this.val($(this).attr('rel'));
      $list.hide();
      /* alert($this.val()); Uncomment this for demonstration! */
    }); // Hides the unordered list when clicking outside of it

    $(document).click(function () {
      $styledSelect.removeClass('active');
      $list.hide();
    });
  });
  $('#view-line').on('click', function (e) {
    e.preventDefault();
    $('#view-grid').removeClass('a-sort__view_active');
    $('#view-line').addClass('a-sort__view_active');
    $('.a-good-card__header').addClass('a-good-line__left');
    $('.a-good-card__header').removeClass('a-good-card__header');
    $('.a-good-card__content').addClass('a-good-line__content');
    $('.a-good-card__content').removeClass('a-good-card__content');
    $('.a-good-card__row-middle').addClass('a-good-line_toggled_row');
    $('.a-good-card__row-middle').removeClass('a-good-card__row');
    $('.a-good-card__row-middle').removeClass('a-good-card__row-middle');
    $('.a-good-card__prices').addClass('a-good-line_toggled_prices');
    $('.a-good-card__prices').removeClass('a-good-card__prices');
    $('.a-good-card__buttons').addClass('a-good-line_toggled_buttons');
    $('.a-good-card').addClass('a-good-line');
    $('.a-good-card').removeClass('a-good-card');
  });
  $('#view-grid').on('click', function (e) {
    e.preventDefault();
    $('#view-line').removeClass('a-sort__view_active');
    $('#view-grid').addClass('a-sort__view_active');
    $('.a-good-line__left').addClass('a-good-card__header');
    $('.a-good-line__left').removeClass('a-good-line__left');
    $('.a-good-line__content').addClass('a-good-card__content');
    $('.a-good-line__content').removeClass('a-good-line__content');
    $('.a-good-line_toggled_row').addClass('a-good-card__row');
    $('.a-good-line_toggled_row').addClass('a-good-card__row-middle');
    $('.a-good-line_toggled_row').removeClass('a-good-line_toggled_row');
    $('.a-good-line_toggled_prices').addClass('a-good-card__prices');
    $('.a-good-line_toggled_prices').removeClass('a-good-line_toggled_prices');
    $('.a-good-card__buttons').removeClass('a-good-line_toggled_buttons');
    $('.a-good-line').addClass('a-good-card');
    $('.a-good-line').removeClass('a-good-line');
  });
  /* objectFitImages(); */
});
//# sourceMappingURL=main.js.map
